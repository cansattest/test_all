/*
 * main.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>
#include <usart.h>
#include <timers.h>
#include <spi.h>
#include <i2c.h>
#include <BMP280.h>
#include <ds18b20.h>
#include <ADXL345.h>
#include <util/delay.h>
#include <nrf.h>

#define F_CPU 1000000UL

int main (void)
{
	uint8_t result[42] = {'$', 'S', 'T', 'A'};
	uint32_t time_val;
	USARTInit();
	TIM1Init ();
	SPIInit ();
	I2CInit();
	_delay_ms (1000);
	RadioConfig (transmitt);
	BMPInit ();
	ADXLInit();
	BMPGetCalib (result + 4);
	while (1){
		TIM1SetVal (0);
		time_val = 0;
		RadioSendCmd (FLUSH_TX, 0, 0, 0);
		RadioSetRegVal (STATUS, 0x7F);
		RadioWriteData (result);
		_delay_ms(50);
		RadioSendCmd (FLUSH_TX, 0, 0, 0);
		RadioSetRegVal (STATUS, 0x7F);
		RadioWriteData (result + 21);
		BMPGetData (result + 28);
		ADXLGetData (result + 34);
		DS18Reset();
		DS18Write (DS18_SKIP_ROM);
		DS18Write (DS18_CONVERT_T);
		_delay_ms (760);
		DS18Reset();
		DS18Write (DS18_SKIP_ROM);
		DS18Write (DS18_READ_SCRATCH);
		result[40] = DS18Read();
		result[41] = DS18Read();
		DS18Reset();
		while (time_val < 1000000UL)
			time_val = TIM1GetVal ();

	}
	return 0;
}
