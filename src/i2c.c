/*
 * i2c.c
 *
 *  Created on: 12.10.2017
 *      Author: annetta897
 */
#include <avr/io.h>
#include <usart.h>

#ifndef F_CPU
	#define F_CPU 1000000UL
#endif

#ifndef I2C_rate
	#define I2C_rate 10000UL
#endif

#define TWBR_val (F_CPU/I2C_rate - 16)/2

void I2CInit (void)
{
//	TWSR = 0;
//	DDRD &=~((1<<0)|(1<<1));
//	PORTD|=(1<<0)|(1<<1);
	TWBR = TWBR_val;
}

void I2CWrite (uint8_t slave, uint8_t addr, uint8_t * data, uint8_t count)
{
	uint8_t i;
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	TWDR = slave<<1;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	TWDR = addr;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	for (i = 0; i < count; i++){
		TWDR = data[i];
		TWCR = (1<<TWINT)|(1<<TWEN);
		while (!(TWCR & (1<<TWINT)))
			;
	}
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
	while ((TWCR & (1<<TWSTO)))
		;
}

void I2CRead (uint8_t slave, uint8_t addr, uint8_t * data, uint8_t count)
{
	uint8_t i;
	TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWSTA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	TWDR = slave<<1;
	TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	TWDR = addr;
	TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN)|(1<<TWSTA);
	while (!(TWCR & (1<<TWINT)))
		;
	TWDR = (slave<<1)|1;
	TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
		;
	for (i = 0; i < count - 1; i++){
		TWCR = (1<<TWINT)|(1<<TWEA)|(1<<TWEN);
		while (!(TWCR & (1<<TWINT)))
			;
		data[i] = TWDR;
	}
	TWCR = (1<<TWINT)|(1<<TWEN);
	while (!(TWCR & (1<<TWINT)))
				;
	data[i] = TWDR;
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
	while ((TWCR & (1<<TWSTO)))
		;
}
