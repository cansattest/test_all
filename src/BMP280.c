/*
 * BMP280.c
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */
#include <spi.h>
#include <i2c.h>
#include <usart.h>

#define BMP_SLAVE_ADDR 0x77

#ifndef BMP_CS
	#define BMP_CS 5
#endif

void BMPInit (void)
{
/*	uint8_t SPIdata[6];
	SPIdata [0] = 0xF4&0x7F;
	SPIdata [1] = 0xFF;
	SPIdata [2] = 0xF5&0x7F;
	SPIdata [3] = 0x20;
	SPISend (SPIdata, SPIdata, 4, BMP_CS);*/
	uint8_t data;
	data = 0xFF;
	I2CWrite (BMP_SLAVE_ADDR, 0xF4, (uint8_t *)&data, 1);
	data = 0x20;
	I2CWrite (BMP_SLAVE_ADDR, 0xF5, (uint8_t *)&data, 1);
}

void BMPGetData (uint8_t * data)
{
/*	uint8_t SPIdata[7];
	uint8_t i;
	SPIdata[0] = 0xF7;
	SPISend (SPIdata, SPIdata, 7, BMP_CS);
	for (i = 0; i < 6; i++)
		data[i] = SPIdata[i+1];*/
	I2CRead (BMP_SLAVE_ADDR, 0xF7, data, 6);
}

void BMPGetCalib (uint8_t * calib)
{
/*	uint8_t i;
	uint8_t tmp[25];
	tmp[0] = 0x88;
	SPISend (tmp, tmp, 25, BMP_CS);
	for (i = 0; i < 24; i++)
		calib[i] = tmp[i+1];*/
	I2CRead (BMP_SLAVE_ADDR, 0x88, calib, 24);
}
