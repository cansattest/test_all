/*
 * ds18b20.h
 *
 *  Created on: 15.09.2017
 *      Author: annetta897
 */

#ifndef HEADERS_DS18B20_H_
#define HEADERS_DS18B20_H_

#define DS18_SKIP_ROM 0xCC
#define DS18_CONVERT_T 0x44
#define DS18_READ_SCRATCH 0xBE

uint8_t DS18Reset (void);
void DS18Write0 (void);
void DS18Write1(void);
void DS18Write (uint8_t);
uint8_t DS18Read (void);

#endif /* HEADERS_DS18B20_H_ */
