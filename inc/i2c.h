/*
 * i2c.h
 *
 *  Created on: 12.10.2017
 *      Author: annetta897
 */

#ifndef I2C_H_
#define I2C_H_

#include <inttypes.h>

void I2CInit (void);
void I2CWrite (uint8_t slave, uint8_t addr, uint8_t * data, uint8_t count);
void I2CRead (uint8_t slave, uint8_t addr, uint8_t * data, uint8_t count);

#endif /* I2C_H_ */
